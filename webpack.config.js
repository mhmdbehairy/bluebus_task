const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/index.js",
    devServer: {
        port: 9000
    },
    output: {
        filename: "build.js",
        path: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "sass-loader"],
                include: path.join(__dirname, "./src")
            },
            {
                test: /\.pug$/,
                include: path.join(__dirname, "./src"),
                loaders: ["pug-loader"]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                include: path.join(__dirname, "./resources/images/"),
                use: ["file-loader"]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                include: path.join(__dirname, "./resources/fonts/"),
                use: ["file-loader"]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({ template: "./src/index.pug", inject: true })
    ]
};  